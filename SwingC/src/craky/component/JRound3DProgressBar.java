package craky.component;

import java.awt.Color;

import javax.swing.BoundedRangeModel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import craky.util.UIResourceManager;
import craky.util.UIUtil;

public class JRound3DProgressBar extends JProgressBar
{
    private static final long serialVersionUID = 1632391629083296112L;
    
    private Color fontColor;
    
    private Color fontCoverClor;
    
    public JRound3DProgressBar()
    {
        this(HORIZONTAL);
    }
    
    public JRound3DProgressBar(int orient)
    {
        this(orient, 0, 100);
    }
    
    public JRound3DProgressBar(int min, int max)
    {
        this(HORIZONTAL, min, max);
    }

    public JRound3DProgressBar(int orient, int min, int max)
    {
        super(orient, min, max);
        init();
    }
    
    public JRound3DProgressBar(BoundedRangeModel newModel)
    {
        super(newModel);
        init();
    }
    
    private void init()
    {
        this.fontColor = Color.ORANGE;
        this.fontCoverClor = UIResourceManager.getWhiteColor();
        setUI(new Round3DProgressBarUI());
        setFont(UIUtil.getDefaultFont());
        setForeground(Color.BLUE);
        setBackground(Color.GRAY);
        setBorder(new EmptyBorder(0, 0, 0, 0));
        setBorderPainted(false);
        setOpaque(false);
    }
    
    @Deprecated
    public void updateUI()
    {}
    
    public Color getFontColor()
    {
        return this.fontColor;
    }

    public void setFontColor(Color fontColor)
    {
        this.fontColor = fontColor;
        this.repaint();
    }

    public Color getFontCoverClor()
    {
        return this.fontCoverClor;
    }

    public void setFontCoverClor(Color fontCoverClor)
    {
        this.fontCoverClor = fontCoverClor;
        this.repaint();
    }
}