package craky.componentc;

import java.awt.Color;

import javax.swing.Icon;
import javax.swing.JLabel;

import craky.util.UIUtil;

public class JCLabel extends JLabel
{
    private static final long serialVersionUID = -736116392575679784L;
    
    private double angle;
    
    private float iconAlpha, textAlpha, backgroundAlpha;
    
    private int deltaX, deltaY;
    
    private Color disabledForeground;
    
    public JCLabel()
    {
        this("", null, LEADING);
    }
    
    public JCLabel(String text)
    {
        this(text, null, LEADING);
    }
    
    public JCLabel(Icon image)
    {
        this(null, image, CENTER);
    }
    
    public JCLabel(String text, int horizontalAlignment)
    {
        this(text, null, horizontalAlignment);
    }
    
    public JCLabel(Icon image, int horizontalAlignment)
    {
        this(null, image, horizontalAlignment);
    }
    
    public JCLabel(String text, Icon icon, int horizontalAlignment)
    {
        super(text, icon, horizontalAlignment);
        angle = 0.0;
        textAlpha = iconAlpha = 1.0f;
        backgroundAlpha = 0.0f;
        disabledForeground = new Color(103, 117, 127);
        setUI(new CLabelUI());
        setBackground(Color.GRAY);
        setForeground(new Color(0, 28, 48));
        setFont(UIUtil.getDefaultFont());
        super.setOpaque(false);
    }
    
    public double getAngle()
    {
        return angle;
    }

    public void setAngle(double angle)
    {
        this.angle = angle;
        this.repaint();
    }

    public float getIconAlpha()
    {
        return iconAlpha;
    }

    public void setIconAlpha(float iconAlpha)
    {
        if(iconAlpha >= 0.0f && iconAlpha <= 1.0f)
        {
            this.iconAlpha = iconAlpha;
            this.repaint();
        }
        else
        {
            throw new IllegalArgumentException("Invalid iconAlpha:" + iconAlpha);
        }
    }

    public float getTextAlpha()
    {
        return textAlpha;
    }

    public void setTextAlpha(float textAlpha)
    {
        if(textAlpha >= 0.0f && textAlpha <= 1.0f)
        {
            this.textAlpha = textAlpha;
            this.repaint();
        }
        else
        {
            throw new IllegalArgumentException("Invalid textAlpha:" + textAlpha);
        }
    }

    public float getBackgroundAlpha()
    {
        return backgroundAlpha;
    }

    public void setBackgroundAlpha(float backgroundAlpha)
    {
        if(backgroundAlpha >= 0.0f && backgroundAlpha <= 1.0f)
        {
            this.backgroundAlpha = backgroundAlpha;
            this.repaint();
        }
        else
        {
            throw new IllegalArgumentException("Invalid backgroundAlpha:" + backgroundAlpha);
        }
    }

    public Color getDisabledForeground()
    {
        return disabledForeground;
    }

    public void setDisabledForeground(Color disabledForeground)
    {
        this.disabledForeground = disabledForeground;
        
        if(!this.isEnabled())
        {
            this.repaint();
        }
    }
    
    public int getDeltaX()
    {
        return deltaX;
    }

    public void setDeltaX(int deltaX)
    {
        this.deltaX = deltaX;
        this.repaint();
    }

    public int getDeltaY()
    {
        return deltaY;
    }

    public void setDeltaY(int deltaY)
    {
        this.deltaY = deltaY;
        this.repaint();
    }
    
    @Deprecated
    public void updateUI()
    {}
    
    @Deprecated
    public void setOpaque(boolean isOpaque)
    {}
}