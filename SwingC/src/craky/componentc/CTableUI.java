package craky.componentc;

import java.awt.Graphics;
import java.lang.reflect.Field;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTableUI;

import craky.util.UIUtil;

public class CTableUI extends BasicTableUI
{
    public static ComponentUI createUI(JComponent table)
    {
        return new CTableUI();
    }

    public void update(Graphics g, JComponent c)
    {
        paintBackground(g, c);
        super.update(g, c);
    }
    
    private void paintBackground(Graphics g, JComponent c)
    {
        if(c instanceof JCTable)
        {
            JCTable table = (JCTable)c;
            UIUtil.paintBackground(g, c, table.getBackground(), table.getBackground(), table.getImage(),
                            table.isImageOnly(), table.getAlpha(), table.getVisibleInsets());
        }
    }
    
    protected void installDefaults()
    {
        try
        {
            Field isFileListField = BasicTableUI.class.getDeclaredField("isFileList");
            isFileListField.setAccessible(true);
            isFileListField.set(this, Boolean.TRUE.equals(table.getClientProperty("Table.isFileList")));
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}